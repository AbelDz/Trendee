package test.itexico.movies.core;

/**
 * @author Abel
 */

public interface Constants {

    // REST APIs and Network related constants ------------------------------------------------------

    String TAKTT_API_CLIENT_ID = "ced53e41aece292b26abcafa954973db812604dbb10bd70c1062879751057b17";
    String TRAKT_API_BASE_URL = "https://api.trakt.tv/";

    // TMDB EXAMPLE: https://api.themoviedb.org/3/movie/550?api_key=f47a598523ed33cc7c8a96f925745f01
    String TMDB_API_API_KEY = "f47a598523ed33cc7c8a96f925745f01";

    int OK_STATUS_CODE = 200;

    int RESOURCE_LOAD_STATUS_SUCCESS = 1;
    int RESOURCE_LOAD_STATUS_LOADING = 0;
    int RESOURCE_LOAD_STATUS_ERROR = -1;

    // UI related constants ------------------------------------------------------------------------

    String PARAM_SHOW_ID_KEY = "ShowId";
    String PARAM_SEASON_KEY = "Season";
}
