package test.itexico.movies.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import test.itexico.movies.model.Episode;
import test.itexico.movies.model.Season;
import test.itexico.movies.repository.EpisodeRepository;
import test.itexico.movies.repository.Resource;
import test.itexico.movies.repository.SeasonRepository;

/**
 * @author Abel
 */

public class SeasonEpisodesViewModel extends ViewModel {

    private EpisodeRepository mEpisodeRepository;

    @Inject
    public SeasonEpisodesViewModel(EpisodeRepository episodeRepository) {
        mEpisodeRepository = episodeRepository;
    }

    public LiveData<Resource<List<Episode>>> fetchSeasonEpisodes(Season season) {
        return fetchSeasonEpisodes(season.getShowId(), season.getId(), season.getNumber());
    }

    public LiveData<Resource<List<Episode>>> fetchSeasonEpisodes(final int showId,
                                                                 final int seasonId,
                                                                 final int seasonNumber) {
        return mEpisodeRepository.getSeasonEpisodes(showId, seasonId, seasonNumber);
    }
}
