package test.itexico.movies.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import test.itexico.movies.model.TrendyShow;
import test.itexico.movies.repository.Resource;
import test.itexico.movies.repository.TrendyShowRepository;

/**
 * @author Abel
 */

public class TrendingShowsViewModel extends ViewModel {

    private TrendyShowRepository mShowRepository;

    @Inject
    public TrendingShowsViewModel(TrendyShowRepository showRepository) {
        mShowRepository = showRepository;
    }

    public LiveData<Resource<List<TrendyShow>>> fetchTrendingShows() {
        return mShowRepository.getAllShows();
    }
}
