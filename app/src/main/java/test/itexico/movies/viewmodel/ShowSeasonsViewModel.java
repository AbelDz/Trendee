package test.itexico.movies.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import test.itexico.movies.model.Season;
import test.itexico.movies.model.TrendyShow;
import test.itexico.movies.repository.Resource;
import test.itexico.movies.repository.SeasonRepository;
import test.itexico.movies.repository.TrendyShowRepository;

/**
 * @author Abel
 */

public class ShowSeasonsViewModel extends ViewModel {

    private SeasonRepository mSeasonRepository;

    @Inject
    public ShowSeasonsViewModel(SeasonRepository seasonRepository) {
        mSeasonRepository = seasonRepository;
    }

    public LiveData<Resource<List<Season>>> fetchShowSeasons(TrendyShow show) {
        return fetchShowSeasons(show.getId());
    }

    public LiveData<Resource<List<Season>>> fetchShowSeasons(final int showId) {
        return mSeasonRepository.getShowSeasons(showId);
    }

}
