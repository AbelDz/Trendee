package test.itexico.movies.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import test.itexico.movies.core.Constants;

/**
 * A generic wrapper class that holds a value with its loading status.
 * @param <T>
 *
 * Credit: This is based on Google's implementation of generic Network-bound resources.
 */
public class Resource<T> {

    @NonNull
    public final int status;

    @Nullable
    public final String message;

    @Nullable
    public final T data;

    public Resource(@NonNull int status, @Nullable T data, @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Resource<?> resource = (Resource<?>) o;

        if (status != resource.status) {
            return false;
        }
        if (message != null ? !message.equals(resource.message) : resource.message != null) {
            return false;
        }
        return data != null ? data.equals(resource.data) : resource.data == null;
    }

    @Override
    public int hashCode() {
        int result = status;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Resource{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    // Public static factory methods to create Resource objects ------------------------------------

    public static <T> Resource<T> createSuccessStatusResource(@Nullable T data) {
        return new Resource<>(Constants.RESOURCE_LOAD_STATUS_SUCCESS, data, null);
    }

    public static <T> Resource<T> createErrorStatusResource(String msg, @Nullable T data) {
        return new Resource<>(Constants.RESOURCE_LOAD_STATUS_ERROR, data, msg);
    }

    public static <T> Resource<T> createLoadingStatusResource(@Nullable T data) {
        return new Resource<>(Constants.RESOURCE_LOAD_STATUS_LOADING, data, null);
    }
}