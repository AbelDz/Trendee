package test.itexico.movies.repository;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;
import test.itexico.movies.api.ApiResponse;
import test.itexico.movies.api.TraktApiRestClient;
import test.itexico.movies.core.AppExecutors;
import test.itexico.movies.core.Constants;
import test.itexico.movies.model.TrendyShow;
import test.itexico.movies.db.TrendyShowDao;

/**
 * @author Abel
 */

@Singleton
public class TrendyShowRepository {

    private TrendyShowDao mShowDao;
    private TraktApiRestClient mApiClient;
    private final AppExecutors mAppExecutors;

    @Inject
    public TrendyShowRepository(TrendyShowDao showDao, TraktApiRestClient apiClient, AppExecutors appExecutors) {
        mShowDao = showDao;
        mApiClient = apiClient;
        mAppExecutors = appExecutors;
    }

    public LiveData<Resource<List<TrendyShow>>> getAllShows() {
        return new NetworkBoundResource<List<TrendyShow>, List<TrendyShow>>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<TrendyShow> trendyShows) {
                mShowDao.insertInTx(trendyShows);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<TrendyShow> trendyShows) {
                return true; // always try to fetch latest, TODO @FutureAbel, KEEP ANALYZING THIS
            }

            @NonNull
            @Override
            protected LiveData<List<TrendyShow>> loadFromDb() {
                return mShowDao.getAllShows();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<TrendyShow>>> createCall() {
                return mApiClient.getTrendingShows();
            }
        }.asLiveData();
    }
}
