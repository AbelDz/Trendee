package test.itexico.movies.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import test.itexico.movies.api.ApiResponse;
import test.itexico.movies.api.TraktApiRestClient;
import test.itexico.movies.core.AppExecutors;
import test.itexico.movies.model.Season;
import test.itexico.movies.db.SeasonDao;

/**
 * @author Abel
 */

public class SeasonRepository {

    private SeasonDao mSeasonDao;
    private TraktApiRestClient mApiClient;
    private final AppExecutors mAppExecutors;

    @Inject
    public SeasonRepository(SeasonDao seasonDao, TraktApiRestClient apiClient, AppExecutors appExecutors) {
        mSeasonDao = seasonDao;
        mApiClient = apiClient;
        mAppExecutors = appExecutors;
    }

    public LiveData<Resource<List<Season>>> getShowSeasons(final int showId) {
        return new NetworkBoundResource<List<Season>, List<Season>>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Season> seasons) {
                for (Season season : seasons) {
                    season.setShowId(showId);
                }
                mSeasonDao.insertInTx(seasons);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Season> seasons) {
                return true; // always try to fetch latest, TODO @FutureAbel, KEEP ANALYZING THIS
            }

            @NonNull
            @Override
            protected LiveData<List<Season>> loadFromDb() {
                return mSeasonDao.getSeasonsByShowId(showId);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Season>>> createCall() {
                return mApiClient.getShowSeasons(showId);
            }
        }.asLiveData();
    }

}
