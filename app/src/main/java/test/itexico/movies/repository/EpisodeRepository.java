package test.itexico.movies.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import test.itexico.movies.api.ApiResponse;
import test.itexico.movies.api.TraktApiRestClient;
import test.itexico.movies.core.AppExecutors;
import test.itexico.movies.db.EpisodeDao;
import test.itexico.movies.db.SeasonDao;
import test.itexico.movies.db.TrendeeRoomDatabase;
import test.itexico.movies.model.Episode;
import test.itexico.movies.model.Season;

/**
 * @author Abel
 */

public class EpisodeRepository {

    private EpisodeDao mEpisodeDao;
    private TraktApiRestClient mApiClient;
    private final AppExecutors mAppExecutors;

    @Inject
    public EpisodeRepository(EpisodeDao episodeDao, TraktApiRestClient apiClient, AppExecutors appExecutors) {
        mEpisodeDao = episodeDao;
        mApiClient = apiClient;
        mAppExecutors = appExecutors;
    }

    // note: There might be a better approach with these parameters, but I'll leave it like this for now.
    public LiveData<Resource<List<Episode>>> getSeasonEpisodes(final int showId,
                                                               final int seasonId,
                                                               final int seasonNumber) {
        return new NetworkBoundResource<List<Episode>, List<Episode>>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<Episode> episodes) {
                for (Episode episode : episodes) {
                    episode.setSeasonId(seasonId);
                }
                mEpisodeDao.insertInTx(episodes);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Episode> episodes) {
                return true; // always try to fetch latest, TODO @FutureAbel, KEEP ANALYZING THIS
            }

            @NonNull
            @Override
            protected LiveData<List<Episode>> loadFromDb() {
                return mEpisodeDao.getEpisodesBySeasonId(seasonId);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Episode>>> createCall() {
                return mApiClient.getSeasonEpisodes(showId, seasonNumber);
            }
        }.asLiveData();
    }
}
