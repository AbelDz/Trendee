package test.itexico.movies.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import test.itexico.movies.model.TrendyShow;

/**
 * @author Abel
 */

@Dao
public interface TrendyShowDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TrendyShow show);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInTx(List<TrendyShow> shows);

    @Query("DELETE FROM trendy_shows")
    void deleteAll();

    @Query("SELECT * FROM trendy_shows ORDER BY watchers DESC")
    LiveData<List<TrendyShow>> getAllShows();

    @Query("SELECT * FROM trendy_shows WHERE id = :showId ORDER BY watchers DESC")
    LiveData<List<TrendyShow>> getShowById(int showId);
}
