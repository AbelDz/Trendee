package test.itexico.movies.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import test.itexico.movies.model.Episode;
import test.itexico.movies.model.Season;

/**
 * @author Abel
 */

@Dao
public interface EpisodeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Episode episode);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInTx(List<Episode> episodes);

    @Query("DELETE FROM episodes")
    void deleteAll();

    @Query("SELECT * FROM episodes ORDER BY season_id DESC")
    LiveData<List<Episode>> getAllEpisodes();

    @Query("SELECT * FROM episodes WHERE season_id = :seasonId ORDER BY number ASC")
    LiveData<List<Episode>> getEpisodesBySeasonId(int seasonId);
}
