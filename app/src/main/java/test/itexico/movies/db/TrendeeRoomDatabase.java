package test.itexico.movies.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

import test.itexico.movies.di.annotation.ApplicationContext;
import test.itexico.movies.model.Episode;
import test.itexico.movies.model.Season;
import test.itexico.movies.model.TrendyShow;

/**
 * @author Abel
 */

@Singleton
@Database(entities = {TrendyShow.class, Season.class, Episode.class}, version = 1)
public abstract class TrendeeRoomDatabase extends RoomDatabase{

    public abstract TrendyShowDao trendyShowDao();
    public abstract SeasonDao seasonDao();
    public abstract EpisodeDao episodeDao();


    // TODO INJECT THESE CALLBACKS
//    private static RoomDatabase.Callback createRoomDatabaseCallback =
//            new RoomDatabase.Callback(){
//
//                @Override
//                public void onCreate (@NonNull SupportSQLiteDatabase db){
//                    super.onCreate(db);
//                    // TODO, INIT STUFF, IN A WORKER THREAD
//                }
//            };
//
//    private static RoomDatabase.Callback openRoomDatabaseCallback =
//            new RoomDatabase.Callback(){
//
//                @Override
//                public void onOpen (@NonNull SupportSQLiteDatabase db){
//                    super.onOpen(db);
//                    // TODO, DO STUFF EACH TIME DB OPENS, IN A WORKER THREAD
//                }
//            };

}
