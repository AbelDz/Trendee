package test.itexico.movies.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import test.itexico.movies.model.Season;
import test.itexico.movies.model.TrendyShow;

/**
 * @author Abel
 */

@Dao
public interface SeasonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Season season);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInTx(List<Season> seasons);

    @Query("DELETE FROM seasons")
    void deleteAll();

    @Query("SELECT * FROM seasons ORDER BY show_id DESC")
    LiveData<List<Season>> getAllSeasons();

    @Query("SELECT * FROM seasons WHERE show_id = :showId ORDER BY number ASC")
    LiveData<List<Season>> getSeasonsByShowId(int showId);
}
