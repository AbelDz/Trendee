package test.itexico.movies.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import test.itexico.movies.api.GsonPostProcessingEnabler;

/**
 * @author Abel
 */

// TODO, I'm cutting corners by implementing Serializable, will implement Parcelable in the future... or not idk
@Entity(tableName = "seasons",
        foreignKeys = @ForeignKey(
                entity = TrendyShow.class,
                parentColumns = "id",
                childColumns = "show_id",
                onDelete = ForeignKey.CASCADE))
public class Season implements GsonPostProcessingEnabler.PostProcessable, Serializable {

    @PrimaryKey
    @NonNull
    private Integer id;

    @ColumnInfo(name = "show_id")
    private Integer showId;

    @SerializedName("number")
    @Expose
    private Integer number;

    @SerializedName("rating")
    @Expose
    private Double rating;

    @SerializedName("ids")
    @Expose
    @Embedded
    private Ids ids;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public Integer getShowId() {
        return showId;
    }

    public void setShowId(Integer showId) {
        this.showId = showId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Ids getIds() {
        return ids;
    }

    public void setIds(Ids ids) {
        this.ids = ids;
    }

    @Override
    public void gsonPostProcess() {
        id = ids.getTrakt();
    }
}
