package test.itexico.movies.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import test.itexico.movies.api.GsonPostProcessingEnabler;

/**
 * @author Abel
 */

// TODO, I'm cutting corners by implementing Serializable, will implement Parcelable in the future... or not idk
@Entity(tableName = "episodes",
        foreignKeys = @ForeignKey(
                entity = Season.class,
                parentColumns = "id",
                childColumns = "season_id",
                onDelete = ForeignKey.CASCADE))
public class Episode implements GsonPostProcessingEnabler.PostProcessable, Serializable {

    @PrimaryKey
    @NonNull
    private Integer id;

    @ColumnInfo(name = "season_id")
    private Integer seasonId;

    @SerializedName("season")
    @Expose
    private Integer season;

    @SerializedName("number")
    @Expose
    private Integer number;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("ids")
    @Expose
    @Embedded
    private Ids ids;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public Integer getSeason() {
        return season;
    }

    public void setSeason(Integer season) {
        this.season = season;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Ids getIds() {
        return ids;
    }

    public void setIds(Ids ids) {
        this.ids = ids;
    }

    @Override
    public void gsonPostProcess() {
        id = ids.getTrakt();
    }
}
