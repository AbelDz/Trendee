package test.itexico.movies.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import test.itexico.movies.api.GsonPostProcessingEnabler;

/**
 * @author Abel
 */

// TODO, I'm cutting corners by implementing Serializable, will implement Parcelable in the future... or not idk
@Entity(tableName = "trendy_shows")
public class TrendyShow implements GsonPostProcessingEnabler.PostProcessable, Serializable {

    @PrimaryKey
    @NonNull
    private Integer id;

    @SerializedName("watchers")
    @Expose
    private Integer watchers;

    @SerializedName("show")
    @Expose
    @Embedded
    private Show show;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public Integer getWatchers() {
        return watchers;
    }

    public void setWatchers(Integer watchers) {
        this.watchers = watchers;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    @Override
    public void gsonPostProcess() {
        id = show.getIds().getTrakt();
    }

    // Nested classes ------------------------------------------------------------------------------

    public static class Show {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("year")
        @Expose
        private Integer year;
        @SerializedName("ids")
        @Expose
        @Embedded
        private Ids ids;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getYear() {
            return year;
        }

        public void setYear(Integer year) {
            this.year = year;
        }

        public Ids getIds() {
            return ids;
        }

        public void setIds(Ids ids) {
            this.ids = ids;
        }
    }


}

