package test.itexico.movies.ui.season;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import test.itexico.movies.R;
import test.itexico.movies.databinding.SeasonGridItemBinding;
import test.itexico.movies.model.Season;

/**
 * @author Abel
 */

public class ShowSeasonsAdapter extends RecyclerView.Adapter<ShowSeasonsAdapter.ShowSeasonsAdapterViewHolder> {

    private Context mContext;
    private List<Season> mSeasons;

    public ShowSeasonsAdapter(@NonNull Context context, @NonNull List<Season> seasons) {
        mContext = context;
        mSeasons = seasons;
    }

    @Override
    public ShowSeasonsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.season_grid_item, parent,
                false);
        return new ShowSeasonsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ShowSeasonsAdapterViewHolder holder, int position) {
        holder.viewHolderBinding.setSeason(mSeasons.get(position));
        // TODO, DO THE GLIDE THING
    }

    @Override
    public int getItemCount() {
        return mSeasons.size();
    }

    public Season getItem(int position) {
        return mSeasons.get(position);
    }

    public void setSeasonList(List<Season> showList) {
        // Clears, re-sets, and updates all elements in the recycler, not optimal but will keep like this for now
        mSeasons.clear();
        mSeasons.addAll(showList);
        notifyDataSetChanged();
    }

    static class ShowSeasonsAdapterViewHolder extends RecyclerView.ViewHolder {
        SeasonGridItemBinding viewHolderBinding;

        public ShowSeasonsAdapterViewHolder(View itemView) {
            super(itemView);
            viewHolderBinding = DataBindingUtil.bind(itemView);
        }
    }
}
