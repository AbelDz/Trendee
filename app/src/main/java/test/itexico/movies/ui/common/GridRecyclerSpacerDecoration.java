package test.itexico.movies.ui.common;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author Abel
 */

public class GridRecyclerSpacerDecoration extends RecyclerView.ItemDecoration {
    private final int mSpaceOffset;

    public GridRecyclerSpacerDecoration(int spaceOffset) {
        this.mSpaceOffset = spaceOffset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = mSpaceOffset;
        outRect.right = mSpaceOffset;
        outRect.bottom = mSpaceOffset;
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = mSpaceOffset;
        }
    }
}
