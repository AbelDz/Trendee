package test.itexico.movies.ui.season;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import test.itexico.movies.R;
import test.itexico.movies.core.Constants;
import test.itexico.movies.databinding.ShowSeasonsFragmentBinding;
import test.itexico.movies.di.component.Injectable;
import test.itexico.movies.model.Season;
import test.itexico.movies.repository.Resource;
import test.itexico.movies.ui.common.NetworkUpdateListener;
import test.itexico.movies.viewmodel.ShowSeasonsViewModel;

/**
 * @author Abel
 */

public class ShowSeasonsFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private ShowSeasonsViewModel mShowSeasonsViewModel;
    private ShowSeasonsFragmentBinding mBinding;
    private int mCurrentShowId = -1;
    private ShowSeasonsAdapter mShowSeasonsAdapter;
    private SeasonSelectListener mSeasonSelectListener;
    private NetworkUpdateListener mNetworkUpdateListener;

    public interface SeasonSelectListener {
        void onSeasonSelected(Season season, int adapterPosition);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = ShowSeasonsFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Show the default action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();

        Bundle args = getArguments();
        if (args != null) {
            mCurrentShowId = args.getInt(Constants.PARAM_SHOW_ID_KEY);
        }

        if (mCurrentShowId != -1) {
            mShowSeasonsViewModel = ViewModelProviders.of(this, viewModelFactory).get(ShowSeasonsViewModel.class);
            mShowSeasonsViewModel.fetchShowSeasons(mCurrentShowId).observe(this, new Observer<Resource<List<Season>>>() {
                @Override
                public void onChanged(@Nullable Resource<List<Season>> seasonListResource) {
                    if (seasonListResource != null) {
                        switch (seasonListResource.status) {
                            case Constants.RESOURCE_LOAD_STATUS_LOADING:
                                // Do nothing for now, leaving placeholder
                                break;
                            case Constants.RESOURCE_LOAD_STATUS_SUCCESS:
                                processDataUpdate(seasonListResource.data);
                                break;
                            case Constants.RESOURCE_LOAD_STATUS_ERROR:
                                processDataUpdate(seasonListResource.data);
                        }
                        if (mNetworkUpdateListener != null) {
                            mNetworkUpdateListener.onNetworkStatusUpdated(seasonListResource.status);
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
        mShowSeasonsAdapter = null;
    }

    public void setNetworkUpdateListener(NetworkUpdateListener networkUpdateListener) {
        mNetworkUpdateListener = networkUpdateListener;
    }

    public void setSeasonSelectListener(SeasonSelectListener seasonSelectListener) {
        mSeasonSelectListener = seasonSelectListener;
    }

    private void processDataUpdate(List<Season> itemUpdates) {
        if (mShowSeasonsAdapter == null) {
            // create the adapter
            if (itemUpdates != null) {
                setupRecyclerView(itemUpdates);
            }
        } else {
            // update the adapter
            updateRecyclerView(itemUpdates);
        }
    }

    private void setupRecyclerView(List<Season> itemUpdates) {
        // Obtaining the RV reference. TODO get from binding directly
        RecyclerView seasonsRecyclerView = mBinding.getRoot().findViewById(R.id.show_seasons_recycler);

        // Applying the LayoutManager to be a Grid.
        seasonsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2,
                GridLayoutManager.VERTICAL, false));

        // Setting divider decorations.
        RecyclerView.ItemDecoration verticalDivider = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        seasonsRecyclerView.addItemDecoration(verticalDivider);

        // Creating and assigning the adapter
        mShowSeasonsAdapter = new ShowSeasonsAdapter(getActivity(), itemUpdates);
        seasonsRecyclerView.setAdapter(mShowSeasonsAdapter);

        // Setting the onClick mechanism
        seasonsRecyclerView.addOnItemTouchListener(new ShowSeasonsRecyclerClickHandler(getActivity(),
                mSeasonSelectListener));
    }

    private void updateRecyclerView(List<Season> seasonList) {
        mShowSeasonsAdapter.setSeasonList(seasonList);
    }

    private class ShowSeasonsRecyclerClickHandler implements RecyclerView.OnItemTouchListener {

        private GestureDetector mmGestureDetector;
        private SeasonSelectListener mmSeasonSelectListener;

        public ShowSeasonsRecyclerClickHandler(Context context, SeasonSelectListener selectListener) {
            mmSeasonSelectListener = selectListener;

            mmGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {

                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (null != child && mmSeasonSelectListener != null && mmGestureDetector.onTouchEvent(e)) {
                if(mmSeasonSelectListener != null){
                    int selectedPosition = rv.getChildLayoutPosition(child);
                    mmSeasonSelectListener.onSeasonSelected(mShowSeasonsAdapter.getItem(selectedPosition),
                            selectedPosition);
                }
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


}
