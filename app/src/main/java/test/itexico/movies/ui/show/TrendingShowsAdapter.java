package test.itexico.movies.ui.show;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import test.itexico.movies.R;
import test.itexico.movies.databinding.TrendingShowGridItemBinding;
import test.itexico.movies.model.TrendyShow;

/**
 * @author Abel
 */

public class TrendingShowsAdapter extends RecyclerView.Adapter<TrendingShowsAdapter.TrendingShowAdapterViewHolder> {

    private Context mContext;
    private List<TrendyShow> mShows;

    public TrendingShowsAdapter(@NonNull Context context, @NonNull List<TrendyShow> shows) {
        mContext = context;
        mShows = shows;
    }

    @Override
    public TrendingShowAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.trending_show_grid_item, parent,
                false);
        return new TrendingShowAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TrendingShowAdapterViewHolder holder, int position) {
        holder.viewHolderBinding.setTrendyShow(mShows.get(position));
        // TODO, DO THE GLIDE THING
    }

    @Override
    public int getItemCount() {
        return mShows.size();
    }

    public TrendyShow getItem(int position) {
        return mShows.get(position);
    }

    public void setShowList(List<TrendyShow> showList) {
        // Clears, re-sets, and updates all elements in the recycler, not optimal but will keep like this for now
        mShows.clear();
        mShows.addAll(showList);
        notifyDataSetChanged();
    }

    static class TrendingShowAdapterViewHolder extends RecyclerView.ViewHolder {
        TrendingShowGridItemBinding viewHolderBinding;

        public TrendingShowAdapterViewHolder(View itemView) {
            super(itemView);
            viewHolderBinding = DataBindingUtil.bind(itemView);
        }
    }
}
