package test.itexico.movies.ui.show;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import test.itexico.movies.R;
import test.itexico.movies.core.Constants;
import test.itexico.movies.databinding.TrendingShowsFragmentBinding;
import test.itexico.movies.di.component.Injectable;
import test.itexico.movies.model.TrendyShow;
import test.itexico.movies.repository.Resource;
import test.itexico.movies.ui.common.NetworkUpdateListener;
import test.itexico.movies.viewmodel.TrendingShowsViewModel;

/**
 * @author Abel
 */

public class TrendingShowsFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private TrendingShowsViewModel mShowViewModel;
    private TrendingShowsFragmentBinding mBinding;
    private TrendingShowsAdapter mTrendingShowsAdapter;
    private ShowSelectListener mShowSelectListener;
    private NetworkUpdateListener mNetworkUpdateListener;

    public interface ShowSelectListener {
        void onShowSelected(TrendyShow show, int adapterPosition);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = TrendingShowsFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Show the default action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();

        mShowViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(TrendingShowsViewModel.class);
        mShowViewModel.fetchTrendingShows().observe(this, new Observer<Resource<List<TrendyShow>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<TrendyShow>> trendyShowListResource) {
                if (trendyShowListResource != null) {
                    switch (trendyShowListResource.status) {
                        case Constants.RESOURCE_LOAD_STATUS_LOADING:
                            // Do nothing for now, leaving placeholder
                            break;
                        case Constants.RESOURCE_LOAD_STATUS_SUCCESS:
                            processDataUpdate(trendyShowListResource.data);
                            break;
                        case Constants.RESOURCE_LOAD_STATUS_ERROR:
                            processDataUpdate(trendyShowListResource.data);
                    }
                    if (mNetworkUpdateListener != null) {
                        mNetworkUpdateListener.onNetworkStatusUpdated(trendyShowListResource.status);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
        mTrendingShowsAdapter = null;
    }

    public void setNetworkUpdateListener(NetworkUpdateListener networkUpdateListener) {
        mNetworkUpdateListener = networkUpdateListener;
    }

    public void setShowSelectListener(ShowSelectListener showSelectListener) {
        mShowSelectListener = showSelectListener;
    }

    private void processDataUpdate(List<TrendyShow> itemUpdates) {
        if (mTrendingShowsAdapter == null) {
            // create the adapter
            if (itemUpdates != null) {
                setupRecyclerView(itemUpdates);
            }
        } else {
            // update the adapter
            updateRecyclerView(itemUpdates);
        }
    }

    private void setupRecyclerView(List<TrendyShow> itemUpdates) {
        // Obtaining the RV reference. TODO get from binding directly
        RecyclerView showsRecyclerView = mBinding.getRoot().findViewById(R.id.trending_shows_recycler);

        // Applying the LayoutManager to be a Grid.
        showsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2,
                GridLayoutManager.VERTICAL, false));

        // Setting divider decorations.
        RecyclerView.ItemDecoration verticalDivider = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        showsRecyclerView.addItemDecoration(verticalDivider);

        // Creating and assigning the adapter
        mTrendingShowsAdapter = new TrendingShowsAdapter(getActivity(), itemUpdates);
        showsRecyclerView.setAdapter(mTrendingShowsAdapter);

        // Setting the onClick mechanism
        showsRecyclerView.addOnItemTouchListener(new TrendingShowRecyclerClickHandler(getActivity(),
                mShowSelectListener));
    }

    private void updateRecyclerView(List<TrendyShow> showList) {
        mTrendingShowsAdapter.setShowList(showList);
    }

    private class TrendingShowRecyclerClickHandler implements RecyclerView.OnItemTouchListener {

        private GestureDetector mmGestureDetector;
        private ShowSelectListener mmShowSelectListener;

        public TrendingShowRecyclerClickHandler(Context context, ShowSelectListener selectListener) {
            mmShowSelectListener = selectListener;

            mmGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {

                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && mmShowSelectListener != null && mmGestureDetector.onTouchEvent(e)) {
                if(mmShowSelectListener != null){
                    int selectedPosition = rv.getChildLayoutPosition(child);
                    mmShowSelectListener.onShowSelected(mTrendingShowsAdapter.getItem(selectedPosition),
                            selectedPosition);
                }
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
