package test.itexico.movies.ui.episode;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import test.itexico.movies.R;
import test.itexico.movies.core.Constants;
import test.itexico.movies.databinding.SeasonEpisodesFragmentBinding;
import test.itexico.movies.databinding.ShowSeasonsFragmentBinding;
import test.itexico.movies.di.component.Injectable;
import test.itexico.movies.model.Episode;
import test.itexico.movies.model.Season;
import test.itexico.movies.model.TrendyShow;
import test.itexico.movies.repository.Resource;
import test.itexico.movies.ui.common.NetworkUpdateListener;
import test.itexico.movies.viewmodel.SeasonEpisodesViewModel;

/**
 * @author Abel
 */

public class SeasonEpisodesFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private SeasonEpisodesViewModel mSeasonEpisodesViewModel;
    private SeasonEpisodesFragmentBinding mBinding;
    private Season mCurrentSeason;
    private SeasonEpisodesAdapter mSeasonEpisodesAdapter;
    private NetworkUpdateListener mNetworkUpdateListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = SeasonEpisodesFragmentBinding.inflate(inflater, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Hide the default action bar
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

        Bundle args = getArguments();
        if (args != null) {
            mCurrentSeason = (Season) args.getSerializable(Constants.PARAM_SEASON_KEY);
        }

        if (mCurrentSeason != null) {
            mBinding.setSeason(mCurrentSeason);

            mSeasonEpisodesViewModel = ViewModelProviders.of(this, viewModelFactory)
                    .get(SeasonEpisodesViewModel.class);
            mSeasonEpisodesViewModel.fetchSeasonEpisodes(mCurrentSeason).observe(this,
                    new Observer<Resource<List<Episode>>>() {
                @Override
                public void onChanged(@Nullable Resource<List<Episode>> episodeListResource) {
                    if (episodeListResource != null) {
                        switch (episodeListResource.status) {
                            case Constants.RESOURCE_LOAD_STATUS_LOADING:
                                // Do nothing for now, leaving placeholder
                                break;
                            case Constants.RESOURCE_LOAD_STATUS_SUCCESS:
                                processDataUpdate(episodeListResource.data);
                                break;
                            case Constants.RESOURCE_LOAD_STATUS_ERROR:
                                processDataUpdate(episodeListResource.data);
                        }
                        if (mNetworkUpdateListener != null) {
                            mNetworkUpdateListener.onNetworkStatusUpdated(episodeListResource.status);
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
        mSeasonEpisodesAdapter = null;
    }

    public void setNetworkUpdateListener(NetworkUpdateListener networkUpdateListener) {
        mNetworkUpdateListener = networkUpdateListener;
    }

    private void processDataUpdate(List<Episode> itemUpdates) {
        if (mSeasonEpisodesAdapter == null) {
            // create the adapter
            if (itemUpdates != null) {
                setupRecyclerView(itemUpdates);
            }
        } else {
            // update the adapter
            updateRecyclerView(itemUpdates);
        }
    }

    private void setupRecyclerView(List<Episode> episodeList) {
        // Obtaining the RV reference.
        RecyclerView seasonsRecyclerView = mBinding.seasonEpisodesRecycler;

        // Applying the LayoutManager to be a Grid.
        seasonsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Creating and assigning the adapter
        mSeasonEpisodesAdapter = new SeasonEpisodesAdapter(getActivity(), episodeList);
        seasonsRecyclerView.setAdapter(mSeasonEpisodesAdapter);
    }

    private void updateRecyclerView(List<Episode> episodeList) {
        mSeasonEpisodesAdapter.setEpisodeList(episodeList);
    }
}
