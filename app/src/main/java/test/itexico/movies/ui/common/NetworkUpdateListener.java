package test.itexico.movies.ui.common;

/**
 * @author Abel
 */

public interface NetworkUpdateListener {

    void onNetworkStatusUpdated(int networkStatus);
}
