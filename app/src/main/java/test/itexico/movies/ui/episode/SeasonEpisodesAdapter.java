package test.itexico.movies.ui.episode;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import test.itexico.movies.R;
import test.itexico.movies.databinding.EpisodeListItemBinding;
import test.itexico.movies.model.Episode;

/**
 * @author Abel
 */

public class SeasonEpisodesAdapter extends RecyclerView.Adapter<SeasonEpisodesAdapter.SeasonEpisodesAdapterViewHolder>{

    private Context mContext;
    private List<Episode> mEpisodes;

    public SeasonEpisodesAdapter(@NonNull Context context, @NonNull List<Episode> episodes) {
        mContext = context;
        mEpisodes = episodes;
    }

    @Override
    public SeasonEpisodesAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.episode_list_item, parent,
                false);
        return new SeasonEpisodesAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SeasonEpisodesAdapterViewHolder holder, int position) {
        holder.viewHolderBinding.setEpisode(mEpisodes.get(position));
    }

    @Override
    public int getItemCount() {
        return mEpisodes.size();
    }

    public Episode getItem(int position) {
        return mEpisodes.get(position);
    }

    public void setEpisodeList(List<Episode> showList) {
        // Clears, re-sets, and updates all elements in the recycler, not optimal but will keep like this for now
        mEpisodes.clear();
        mEpisodes.addAll(showList);
        notifyDataSetChanged();
    }

    static class SeasonEpisodesAdapterViewHolder extends RecyclerView.ViewHolder {
        EpisodeListItemBinding viewHolderBinding;

        public SeasonEpisodesAdapterViewHolder(View itemView) {
            super(itemView);
            viewHolderBinding = DataBindingUtil.bind(itemView);
        }
    }
}
