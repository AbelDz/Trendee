package test.itexico.movies.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import test.itexico.movies.R;
import test.itexico.movies.core.Constants;
import test.itexico.movies.databinding.HomeActivityBinding;
import test.itexico.movies.model.Season;
import test.itexico.movies.model.TrendyShow;
import test.itexico.movies.ui.common.NetworkUpdateListener;
import test.itexico.movies.ui.episode.SeasonEpisodesFragment;
import test.itexico.movies.ui.season.ShowSeasonsFragment;
import test.itexico.movies.ui.show.TrendingShowsFragment;

/**
 * @author Abel
 */

public class HomeActivity extends AppCompatActivity implements HasSupportFragmentInjector,
        NetworkUpdateListener,
        TrendingShowsFragment.ShowSelectListener,
        ShowSeasonsFragment.SeasonSelectListener {

    private final static String FRAGMENT_TAG_SHOWS = "TrendingShowsFragment";
    private final static String FRAGMENT_TAG_SEASONS = "ShowSeasonFragment";
    private final static String FRAGMENT_TAG_EPISODES = "SeasonEpisodesFragment";

    @Inject
    DispatchingAndroidInjector<Fragment> mDispatchingAndroidInjector;
    HomeActivityBinding mBinding;

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return mDispatchingAndroidInjector;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.home_activity);
        setSupportActionBar(mBinding.appToolbar);

        goToTrendingShows();
    }

    private void goToTrendingShows() {
        TrendingShowsFragment showsFragment = new TrendingShowsFragment();
        showsFragment.setShowSelectListener(this);
        showsFragment.setNetworkUpdateListener(this);
        navigateToFragment(showsFragment, FRAGMENT_TAG_SHOWS);
    }

    private void goToShowSeasons(TrendyShow show) {
        ShowSeasonsFragment seasonsFragment = new ShowSeasonsFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.PARAM_SHOW_ID_KEY, show.getId()); // TODO make parcelable
        seasonsFragment.setArguments(args);
        seasonsFragment.setSeasonSelectListener(this);
        seasonsFragment.setNetworkUpdateListener(this);
        navigateToFragment(seasonsFragment, FRAGMENT_TAG_SEASONS);
    }

    private void goToSeasonEpisodes(Season season) {
        SeasonEpisodesFragment episodesFragment = new SeasonEpisodesFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.PARAM_SEASON_KEY, season); // TODO make parcelable
        episodesFragment.setArguments(args);
        episodesFragment.setNetworkUpdateListener(this);
        navigateToFragment(episodesFragment, FRAGMENT_TAG_EPISODES);
    }

    private void navigateToFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, tag)
                .addToBackStack(tag) // TODO IMPLEMENT PROPER BACK NAV
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            this.finish();
        }
    }

    @Override
    public void onNetworkStatusUpdated(int networkStatus) {
        int networkUpdateTextRes = -1;
        int networkUpdateBgRes = -1;
        switch (networkStatus) {
            case Constants.RESOURCE_LOAD_STATUS_LOADING:
                networkUpdateTextRes = R.string.network_update_label_updating;
                networkUpdateBgRes = R.drawable.round_blue_bg;
                break;
            case Constants.RESOURCE_LOAD_STATUS_SUCCESS:
                networkUpdateTextRes = R.string.network_update_label_success;
                networkUpdateBgRes = R.drawable.round_green_bg;
                break;
            case Constants.RESOURCE_LOAD_STATUS_ERROR:
                networkUpdateTextRes = R.string.network_update_label_failed;
                networkUpdateBgRes = R.drawable.round_gray_bg;
        }
        if (networkUpdateTextRes != -1) {
            mBinding.networkStatusLabel.setText(networkUpdateTextRes);
            mBinding.networkStatusLabel.setBackgroundResource(networkUpdateBgRes);
            mBinding.networkStatusLabel.setVisibility(View.VISIBLE);
            if (networkStatus == Constants.RESOURCE_LOAD_STATUS_SUCCESS) {
                mBinding.networkStatusLabel.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.networkStatusLabel.setVisibility(View.GONE);
                    }
                }, 2000);
            }
        }
    }

    @Override
    public void onShowSelected(TrendyShow show, int adapterPosition) {
        goToShowSeasons(show);
    }

    @Override
    public void onSeasonSelected(Season season, int adapterPosition) {
        goToSeasonEpisodes(season);
    }
}
