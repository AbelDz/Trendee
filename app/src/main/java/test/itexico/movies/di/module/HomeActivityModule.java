package test.itexico.movies.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import test.itexico.movies.ui.HomeActivity;

/**
 * @author Abel
 */

@Module
public abstract class HomeActivityModule {
    @Singleton
    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract HomeActivity contributeHomeActivity();
}
