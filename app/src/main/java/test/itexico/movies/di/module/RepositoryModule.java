package test.itexico.movies.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import test.itexico.movies.api.LiveDataCallAdapterFactory;
import test.itexico.movies.core.AppExecutors;
import test.itexico.movies.core.Constants;
import test.itexico.movies.db.SeasonDao;
import test.itexico.movies.di.annotation.TrendeeApplicationScope;
import test.itexico.movies.model.Episode;
import test.itexico.movies.model.Season;
import test.itexico.movies.db.TrendeeRoomDatabase;
import test.itexico.movies.model.TrendyShow;
import test.itexico.movies.db.TrendyShowDao;
import test.itexico.movies.api.GsonPostProcessingEnabler;
import test.itexico.movies.api.TraktApiRestClient;
import test.itexico.movies.repository.SeasonRepository;
import test.itexico.movies.repository.TrendyShowRepository;

/**
 * @author Abel
 */

@Module(includes = TrendeeRoomDatabaseModule.class)
public class RepositoryModule {

    @TrendeeApplicationScope
    @Provides
    public SeasonRepository seasonRepository(SeasonDao seasonDao, TraktApiRestClient apiClient,
                                             AppExecutors appExecutors) {
        return new SeasonRepository(seasonDao, apiClient, appExecutors);
    }

    @TrendeeApplicationScope
    @Provides
    public TrendyShowRepository trendyShowRepository(TrendyShowDao showDao, TraktApiRestClient apiClient,
                                                     AppExecutors appExecutors) {
        return new TrendyShowRepository(showDao, apiClient, appExecutors);
    }

    @TrendeeApplicationScope
    @Provides
    public TraktApiRestClient traktApiEndpointInterface(GsonConverterFactory gsonConverterFactory){
        return new Retrofit.Builder()
                .baseUrl(Constants.TRAKT_API_BASE_URL)
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .addConverterFactory(gsonConverterFactory)
                .build()
                .create(TraktApiRestClient.class);
    }

    @Provides
    public GsonConverterFactory gsonConverterFactory(Gson gson){
        return GsonConverterFactory.create(gson);
    }

    @Provides
    public Gson gson(){
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .registerTypeAdapterFactory(new GsonPostProcessingEnabler<TrendyShow>())
                .registerTypeAdapterFactory(new GsonPostProcessingEnabler<Season>()) // too lazy to inject these
                .registerTypeAdapterFactory(new GsonPostProcessingEnabler<Episode>())
                .create();
    }

}
