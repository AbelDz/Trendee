package test.itexico.movies.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import test.itexico.movies.di.annotation.ViewModelKey;
import test.itexico.movies.viewmodel.SeasonEpisodesViewModel;
import test.itexico.movies.viewmodel.ShowSeasonsViewModel;
import test.itexico.movies.viewmodel.TrendeeViewModelFactory;
import test.itexico.movies.viewmodel.TrendingShowsViewModel;


/**
 * @author Abel
 */

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(TrendingShowsViewModel.class)
    abstract ViewModel bindTrendingShowsViewModel(TrendingShowsViewModel trendingShowsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ShowSeasonsViewModel.class)
    abstract ViewModel bindSeasonsViewModel(ShowSeasonsViewModel seasonsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SeasonEpisodesViewModel.class)
    abstract ViewModel bindSeasonEpisodesViewModel(SeasonEpisodesViewModel showEpisodesViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(TrendeeViewModelFactory factory);
}
