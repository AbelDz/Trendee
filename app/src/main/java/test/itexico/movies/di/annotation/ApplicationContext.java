package test.itexico.movies.di.annotation;

import javax.inject.Qualifier;

/**
 * @author Abel
 */

@Qualifier
public @interface ApplicationContext {
}
