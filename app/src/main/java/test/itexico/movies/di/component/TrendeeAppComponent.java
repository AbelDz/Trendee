package test.itexico.movies.di.component;

import android.app.Application;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import test.itexico.movies.core.TrendeeApplication;
import test.itexico.movies.di.annotation.TrendeeApplicationScope;
import test.itexico.movies.di.module.HomeActivityModule;
import test.itexico.movies.di.module.RepositoryModule;
import test.itexico.movies.di.module.ViewModelModule;
import test.itexico.movies.viewmodel.TrendingShowsViewModel;

/**
 * @author Abel
 */

@TrendeeApplicationScope
@Component(modules = {
        AndroidInjectionModule.class,
        RepositoryModule.class,
        ViewModelModule.class,
        HomeActivityModule.class})
public interface TrendeeAppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        TrendeeAppComponent build();
    }

    void inject(TrendeeApplication trendeeApp);
}

