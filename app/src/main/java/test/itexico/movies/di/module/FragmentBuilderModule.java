package test.itexico.movies.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import test.itexico.movies.ui.episode.SeasonEpisodesFragment;
import test.itexico.movies.ui.season.ShowSeasonsFragment;
import test.itexico.movies.ui.show.TrendingShowsFragment;

/**
 * @author Abel
 */

@Module
public abstract class FragmentBuilderModule {
    @ContributesAndroidInjector
    abstract TrendingShowsFragment contributeTrendingShowsFragment();

    @ContributesAndroidInjector
    abstract ShowSeasonsFragment contributeShowSeasonsFragment();

    @ContributesAndroidInjector
    abstract SeasonEpisodesFragment contributeSeasonEpisodesFragment();
}
