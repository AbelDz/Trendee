package test.itexico.movies.di.module;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import test.itexico.movies.di.annotation.ApplicationContext;
import test.itexico.movies.di.annotation.TrendeeApplicationScope;
import test.itexico.movies.db.EpisodeDao;
import test.itexico.movies.db.SeasonDao;
import test.itexico.movies.db.TrendeeRoomDatabase;
import test.itexico.movies.db.TrendyShowDao;

/**
 * @author Abel
 */

@Module
public class TrendeeRoomDatabaseModule {

    @TrendeeApplicationScope
    @Provides
    public TrendeeRoomDatabase trendeeRoomDatabase(Application application) {
        return Room.databaseBuilder(application,
                TrendeeRoomDatabase.class, "trendee_database")
//                .addCallback(createRoomDatabaseCallback) // TODO ADD CALLBACKS
//                .addCallback(openRoomDatabaseCallback)
                .build();
    }

    @TrendeeApplicationScope
    @Provides
    public TrendyShowDao trendyShowDao(TrendeeRoomDatabase trendeeRoomDatabase) {
        return trendeeRoomDatabase.trendyShowDao();
    }

    @TrendeeApplicationScope
    @Provides
    public SeasonDao seasonDao(TrendeeRoomDatabase trendeeRoomDatabase) {
        return trendeeRoomDatabase.seasonDao();
    }

    @TrendeeApplicationScope
    @Provides
    public EpisodeDao episodeDao(TrendeeRoomDatabase trendeeRoomDatabase) {
        return trendeeRoomDatabase.episodeDao();
    }
}
