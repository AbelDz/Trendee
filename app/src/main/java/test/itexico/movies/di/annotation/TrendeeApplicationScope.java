package test.itexico.movies.di.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * @author Abel
 */

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface TrendeeApplicationScope {
}
