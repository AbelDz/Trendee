package test.itexico.movies.api;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * @author Abel
 */

public class GsonPostProcessingEnabler<T> implements TypeAdapterFactory {

    public interface PostProcessable {
        void gsonPostProcess();
    }

    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);

        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            @Override
            public T read(JsonReader in) throws IOException {
                T obj = delegate.read(in);
                if (obj instanceof GsonPostProcessingEnabler.PostProcessable) {
                    ((PostProcessable) obj).gsonPostProcess();
                }
                return obj;
            }
        };
    }
}
