package test.itexico.movies.api;

import android.arch.lifecycle.LiveData;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import test.itexico.movies.model.Episode;
import test.itexico.movies.model.Season;
import test.itexico.movies.model.TrendyShow;

/**
 * @author Abel
 */

@Singleton
public interface TraktApiRestClient {

    @Headers({
            "Content-Type: application/json",
            "trakt-api-key: ced53e41aece292b26abcafa954973db812604dbb10bd70c1062879751057b17",
            "trakt-api-version: 2"
    })
    @GET("shows/trending")
    LiveData<ApiResponse<List<TrendyShow>>> getTrendingShows();

    @Headers({
            "Content-Type: application/json",
            "trakt-api-key: ced53e41aece292b26abcafa954973db812604dbb10bd70c1062879751057b17",
            "trakt-api-version: 2"
    })
    @GET("shows/{traktShowId}/seasons?extended=full")
    LiveData<ApiResponse<List<Season>>> getShowSeasons(@Path("traktShowId") int traktShowId);

    @Headers({
            "Content-Type: application/json",
            "trakt-api-key: ced53e41aece292b26abcafa954973db812604dbb10bd70c1062879751057b17",
            "trakt-api-version: 2"
    })
    @GET("shows/{traktShowId}/seasons/{season}")
    LiveData<ApiResponse<List<Episode>>> getSeasonEpisodes(@Path("traktShowId") int traktShowId,
                                                           @Path("season") int season);
}
